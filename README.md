# To start the project

```shell
cd my-app
npm install
npm start
```


Available pages:

- `/login`
- `/password-reset` (not yet implemented)