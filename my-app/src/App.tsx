import React from 'react';
import './App.css';
import Router from "./commons/Router";
import LoginPage from './pages/login/LoginPage';
import NotFoundPage from "./pages/notFound/NotFoundPage";

function App() {
  return (
    <Router
      notFoundComponent={<NotFoundPage/>}
      routes={[
        {url: '/login', component: <LoginPage/>}
      ]}
    />
  );
}

export default App;
