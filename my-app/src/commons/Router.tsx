import {ReactNode} from "react";

type RouteType = {
  url: string,
  component: JSX.Element;
}

type RouterProps = {
  routes: RouteType[],
  notFoundComponent: JSX.Element
}

export default function Router({routes, notFoundComponent}: RouterProps): JSX.Element {
  for (let elem of routes) {
    if (window.location.pathname.endsWith(elem.url)) {
      return elem.component;
    }
  }
  return notFoundComponent;
}


export function Link({to, children}: { to: string, children: ReactNode }) {
  return <a href={process.env.PUBLIC_URL + to}>
    {children}
  </a>
}