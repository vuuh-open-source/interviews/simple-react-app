import React from 'react';
import {Link} from "../../commons/Router";


export default function NotFoundPage() {
  return <div className={'container'}>
    <div className="row">
      <div className="col text-center">
        <h1>Page not found</h1>
        <h2>Go back to the <Link to="/Login">Login page</Link></h2>
      </div>
    </div>
  </div>
}