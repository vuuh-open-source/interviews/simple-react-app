import React from 'react';
import {Link} from "../../commons/Router";


export default function LoginPage() {

  function onSubmit(event: React.FormEvent<HTMLFormElement>) {
    event.preventDefault();
  }

  function onShowPressed(event: React.FormEvent<HTMLButtonElement>) {
    event.preventDefault();
  }

  return <div className={'container'}>
    <div className="row">
      <div className="col-4 offset-4">
        <div className="card mt-5">
          <div className="card-header">
            <h4>Login</h4>
          </div>
          <div className="card-body">
            <form action="" onSubmit={onSubmit}>

              <div className="form-group">
                <label htmlFor="" className="form-label">Email</label>
                <input type="email" className="form-control"/>
              </div>

              <div className="form-group">
                <label htmlFor="" className="form-label">Password</label>
                <div className="row">
                  <div className="col-8">

                    <input type="text" className="form-control"/>
                  </div>
                  <div className="col">
                    <button className="btn btn-primary w-100" onClick={onShowPressed}>Show</button>
                  </div>
                </div>
              </div>

              <button className={'btn btn-primary w-100 mt-3'}>
                Login
              </button>

              <hr/>

              <Link to="/password-reset">
                Forgot password
              </Link>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
}